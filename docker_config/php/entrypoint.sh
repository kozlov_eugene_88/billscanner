#!/bin/sh

`cd /var/www/assist`
`php /var/www/assist/composer.phar install`
`php artisan key:generate`

chmod 777 -Rf /var/www/assist/storage/
chmod 777 -Rf /var/www/assist/bootstrap/cache/
chmod 777 -Rf /var/www/logs-assist/

exec "$@";