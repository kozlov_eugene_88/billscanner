## Запуск проекта (нужен докер)
1. Первый. 'make build' OR 'docker-compose up --build'
2. Последующие. 'make up' OR 'docker-compose up -d'

---

## Описание
В проекте используется 2 OCR для распознования текста из документов или фото. Первая - сервис Convertio. Вторая
внутренний компонент на java ApacheTika. Включить или выключить любой из них можно в .env. По умолчанию стоит
Convertio. Второй используется как запасной. Я его использовал, т.к. возникли проблемы с кодировкам при работе
со сторонними сервисами OCR (вероятно из-за использования windows). Внутренний сервис (ApacheTika) показывает большее
быстродействие и не имел проблем с кодировками для меня.

---

## Инструкция
1. Скачать проект
2. Запустить docker-compose как указано выше. Последним загрузится nodejs/php.
Дождаться уведомпления, что исходники css|js скомпилированны и
установлены все зависимости php. Только после этого открывать http://localhost
3. В env меняя CONVERTIO_ENABLED меняется сервис OCR (не забываем сбросить настройки)
4. Загруженные файлы сохраняются в storage/temp. Для очистки файлов старше 10 минут используйте
команду "php artisan temp:clean"