<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DeleteTempFiles extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'temp:clean';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистить временные файлы из storage/app/temp';
    protected $lifetime;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->lifetime = config('filesystems.disks.temp.lifetime', 60 * 24);
    }

    /**
     * Удаление временных файлов из storage/app/temp, которые старше $lifetime (1 день по умолчанию)
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::allFiles('temp');
        $now   = Carbon::now();
        $i = 0;
        foreach ($files as $file) {
            $lastModified = Carbon::createFromTimestamp(Storage::lastModified($file));
            $diff         = $lastModified->diffInMinutes($now);
            if ($diff > $this->lifetime) {
                Storage::delete($file);
                $i++;
            }
        }
        $this->info("Файлов удалено: $i, которые старше $this->lifetime минут");
    }
}
