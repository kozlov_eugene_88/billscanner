<?php

namespace App\Services\WordSearcherService;

class WordSearcher
{

    const UNP     = 'unp';
    const ACCOUNT = 'account';
    const AMOUNT  = 'amount';
    /**
     * Хранение всех найденных совпадений в этом массиве
     * @var array
     */
    public $container;
    /**
     * Обработанная строка (одна) склеенная через пробел. Используется для поиска.
     * @var string
     */
    public $text;
    /**
     * Необработанный текст, пришедший с ORC.
     * @var string
     */
    public $rawString;

    /**
     * Если сырая строка не пустая, то очищает от пробелов, пустых строк. Затем склеивает по пробелу
     * @param string $text
     */
    public function __construct(string $text = '')
    {
        $this->rawString = $text;
        $this->text      = '';
        $this->container = [];
        if ($this->rawString) {
            $arr        = explode("\n", $this->rawString);
            $collect    = collect($arr)->map(function ($str) {
                return trim($str);
            })->filter()->toArray();
            $this->text = implode(' ', $collect);
        }
    }

    /**
     * @return WordSearcher
     */
    public function findUnp(): self
    {
        if ($this->text) {
            $matches = [];
            preg_match('%(П|:|\s)(\d{9})(\D{1})%iu', $this->text, $matches);
            if (isset($matches[2])) {
                $this->container[self::UNP] = $matches[2];
            }
        }
        return $this;
    }

    /**
     * @return WordSearcher
     */
    public function findAccount(): self
    {
        if ($this->text) {
            $matches = [];
            preg_match('%BY\d{2}[A-Z]{4}\d{20}%iu', $this->text, $matches);
            if (isset($matches[0])) {
                $this->container[self::ACCOUNT] = $matches[0];
            }
        }
        return $this;
    }

    /**
     * Поиск итоговой суммы. Ищет все дробные числа и выбирает максимальное число из них.
     * В большинстве случаев в счете, итоговая сумма будет максимальной цифрой.
     * @return WordSearcher
     */
    public function findAmount(): self
    {
        if ($this->text) {
            $matches = [];
            preg_match_all('%(\d{1,3} )?\d+\,\d{2}%iu', $this->text, $matches);
            $max = collect($matches[0])->map(function ($value) {
                return floatval(str_replace([' ', ','], ['', '.'], $value));
            })->max();
            if ($max) {
                $this->container[self::AMOUNT] = $max;
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->container;
    }
}
