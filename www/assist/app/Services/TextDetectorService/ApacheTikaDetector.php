<?php

namespace App\Services\TextDetectorService;

use App\Services\Contracts\Detector;
use Vaites\ApacheTika\Client;

class ApacheTikaDetector implements Detector
{

    public $client;

    public function __construct()
    {
        $this->client = Client::make(
            config('textdetector.detectors.apachetika.host'),
            config('textdetector.detectors.apachetika.port')
        );
    }

    public function detect(string $filepath): ?string
    {
        try {
            return $this->client->getText($filepath);
        } catch (\Throwable $e) {
            return null;
        }

    }
}
