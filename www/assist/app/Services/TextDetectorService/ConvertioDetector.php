<?php

namespace App\Services\TextDetectorService;

use App\Services\Contracts\Detector;
use Convertio\Convertio;

class ConvertioDetector implements Detector
{

    public $client;

    public function __construct()
    {
        $this->client = new Convertio(config('textdetector.detectors.convertio.api_key'));
    }

    public function detect(string $filepath): ?string
    {
        try {
            return $this->client->start($filepath, 'txt')
                ->wait()->fetchResultContent()->result_content;
        } catch (\Throwable $e) {
            return null;
        } finally {
            $this->client->delete();
        }

    }
}
