<?php

namespace App\Services\Contracts;

interface Detector
{
    public function detect(string $filepath) :?string;
}
