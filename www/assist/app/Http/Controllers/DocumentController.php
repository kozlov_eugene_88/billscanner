<?php

namespace App\Http\Controllers;

use App\Http\Requests\DocumentRequest;
use App\Services\Contracts\Detector;
use App\Services\WordSearcherService\WordSearcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{

    /**
     * см. TextDetectorServiceProvider
     * @var Detector
     */
    protected $detector;

    public function __construct(Detector $detector)
    {
        $this->detector = $detector;
    }

    /**
     * Метод на получение данных из загруженного файла
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(DocumentRequest $request)
    {
        //кидаем файл в папку на сервере (очистка командой или по крону)
        $shortPath = $request->file('document')->store('temp');
        //отправляем файл в OCR для получения текста
        $text = $this->detector->detect(Storage::path($shortPath));
        //ищем в полученном тексте нужные данные, получаем на выходе массив
        $params = (new WordSearcher($text))->findUnp()->findAccount()->findAmount()->get();
        return response()->json($params);
    }
}

