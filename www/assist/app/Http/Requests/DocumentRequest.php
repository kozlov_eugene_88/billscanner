<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'document' => 'required|file|max:1000|mimes:jpeg,jpg,png,pdf,doc,docx'
        ];
    }

    public function messages() {
        return [
            'required' => 'Ты забыл загрузить файлик для сканирования!',
            'file'     => 'Кажись тут должен быть файл',
            'max'      => 'Файл больше одного метра не потянем...',
            'mimes'    => 'Подходит только jpeg,jpg,png,pdf,doc,docx',
        ];
    }
}
