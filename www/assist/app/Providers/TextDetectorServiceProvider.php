<?php

namespace App\Providers;

use App\Services\Contracts\Detector;
use App\Services\TextDetectorService\ApacheTikaDetector;
use App\Services\TextDetectorService\ConvertioDetector;
use Illuminate\Support\ServiceProvider;

class TextDetectorServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(Detector::class, function($app) {
            //различные условия... (например, пинг)
            if (config('textdetector.detectors.convertio.enabled')){
                return new ConvertioDetector();
            } else return new ApacheTikaDetector();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [Detector::class];
    }
}
