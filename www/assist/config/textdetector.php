<?php

return [

    /*
    |--------------------------------------------------------------------------
    | config
    |--------------------------------------------------------------------------
    |
    | Настройки конвертеров/детекторов текста
    |
    | Convertio - https://developers.convertio.co/ru/api/docs/
    |
    | ApacheTika (как альтернатива, если основной недоступен):
    | 1. пакет для работы https://packagist.org/packages/vaites/php-apache-tika,
    | 2. localhost:9998 [GET] - подробности о методах,
    |
    */

    'detectors' => [

        'convertio' => [
            'enabled' => env('CONVERTIO_ENABLED', 'true'),
            'api_key' => env('CONVERTIO_API', 'api_key'),
        ],

        'apachetika' => [
            'enabled' => env('APACHETIKA_ENABLED', 'true'),
            'host'    => env('APACHETIKA_HOST', 'assist-java'),
            'port'    => env('APACHETIKA_PORT', '9998'),
        ],
    ],

];
